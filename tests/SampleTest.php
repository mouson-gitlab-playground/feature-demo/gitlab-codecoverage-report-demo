<?php

namespace Tests;

use Mouson\Template\Sample;
use PHPUnit\Framework\TestCase;

class SampleTest extends TestCase
{
    protected function setUp(): void
    {
    }

    protected function tearDown(): void
    {
    }

    public function testNot(): void
    {
        $target = new Sample();
        $this->assertTrue($target->not(false));
        $this->assertFalse($target->not(true));
    }

}
